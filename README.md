# EP_live_template

Entorno de pruebas para testear el comportamiento de las story cards en clientes de correo


Instalación

```sh
 npm install gulp-cli -g
```

A continuación instalamos el resto de los paquetes

```sh
 npm install
```


### Previsualización de plantillas

  - Para poder ver en tiempo real las modificaciones de las piezas ejecutamos en el directorio:
  
  ```sh
   gulp
  ```

  - Una vez ejecutado si no se inicia un navegador automaticemente abrir una pestaña 
  
  ```sh
   http://localhost:3000/
  ```


**Enlaces piezas**

https://ethereal.email/message/XbsBLHdneCuionJVXbsBLddc9CwJsG6bAAAAAdLiK9MklLR8YTgE-TW-1rI?tab=header



