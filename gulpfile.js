/**generador de templates**/

const config = require('./conf');
const {src, dest, series, watch} = require('gulp');
const browserSync = require('browser-sync').create();
const fileinclude = require('gulp-file-include');
const imagemin = require('gulp-imagemin');

/**Copiamos el contenido modificado y actualizamos el browser**/

console.log('config:',config);

/*Copiamos las carpeta*/
const copy = (cb) => {

    console.log('copy ...', cb);

    return src([config.input])
        .pipe(dest(config.output))
};


/*Parseamos el archivo*/
const parseFile = (cb) => {
    console.log('parseFile ...', cb);


    return src([config.template])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: config.path
        }))
        .pipe(dest(config.output))
        .pipe(browserSync.stream());

};


exports.copy = copy;
exports.parseFile = parseFile;

//exports.vista= watch('private/*',copy());

exports.default = function () {

    browserSync.init({
        server: config.output
    });

    copy();
    parseFile();

    // You can use a single task
    watch(config.input, series(copy, parseFile));
};
